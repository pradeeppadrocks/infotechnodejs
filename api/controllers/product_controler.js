const Product = require('../models/product_model')
const mongoose = require('mongoose')
const Gen = require('../modules/module.generic')
const Category = require('../models/category_model')

let productData = {
    create_products: create_products,
    get_All_Products: get_All_Products,
    delete_Products:delete_Products,
    update_Products:update_Products
}

//creating products
function create_products(req, res, next) {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        categoryId: req.body.categoryId
    })

    Product.find({
            name: req.body.name
        })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.json(Gen.responseReturn(409, 'product Already Exists Try Different product', {}))
            } else {
                product.save().then(result => {
                    let product = {
                        name: result.name,
                        price: result.price,
                        _id: result._id,
                        categoryId: result.categoryId,
                    }
                    return res.json(Gen.responseReturn(200, 'product was created', product, true))
                }).catch(err => {
                    return res.json(Gen.responseReturn(500, err, {}))
                })
            }
        })
}


//getting all products
function get_All_Products(req, res, next) {

    const pageSize = req.body.pageSize;
    const pageNo = req.body.pageNo;
    Product.aggregate([{
        $lookup: {
            from: "categories",
            localField: "categoryId",
            foreignField: "_id",
            as: "category"
        }
    }, 
    {
        $project: {
            "_id": 1,
            "name": 1,
            "price": 1,
            "categoryId": 1,
            "categoryName": "$category.categoryName"
        }
    },
      {
        $facet: {
            category: [{
                $skip: (pageNo - 1) * pageSize
            }, {
                $limit: pageSize
            }],

            totalCount: [{
                $count: 'count'
            }]
        }
    } 
]).exec(function (err, data) {
        res.status(200).json(data)
    })


    /*    const pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;
       const pageNo = req.body.pageNo ? parseInt(req.body.pageNo) : 1;
           Product.find()
           .skip((pageNo - 1) * pageSize)
           .limit(pageSize)
            .select('name price _id  categoryId')
           .exec()
           .then(docs => {
               const response = {
                   count: docs.length,
                   products: docs.map(prod => {
                       return {
                           _id: prod._id,
                           name: prod.name,
                           price: prod.price,
                           categoryId: prod.categoryId
                       }
                   })
               }
               if (docs.length >= 0) {
                   res.status(200).json(response)
               } else {
                   res.status(404).json({
                       message: 'No entries found'
                   })
               }
           })
           .catch(err => {
               res.status(500).json({
                   message: 'something went wrong',
                   error: err
               })
           }) */
}

//deleting category with all related products also
function delete_Products(req, res, next) {
    const id = req.params.productId
    Product.deleteOne({
            _id: id
        })
        .exec()
        .then(result => {
            Product.deleteMany({
                    categoryId: id
                })
                .exec()
                .then(result2 => {
                    res.status(200).json(result2)
                })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

function update_Products(req, res, next) {
    const update = new Product({
        name: req.body.name,
        price:req.body.price
    })

    console.log("update",req.body)
    Product.findOneAndUpdate({
        _id: req.body.id
    }, update, {
        new: true
    }, function (err, updated) {
        if (err) {
            return res.json(Gen.responseReturn(409, 'Something went wrong id is not correct', {}))
        } else if (updated) {

            return res.json(Gen.responseReturn(200, 'product updated successfully', {}, true))
        } else {
            return res.json(Gen.responseReturn(200, 'product not found', {}, true))
        }
    })


}

module.exports = productData