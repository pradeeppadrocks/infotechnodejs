const Category = require('../models/category_model')
const Product = require('../models/product_model')
const Gen = require('../modules/module.generic')

const mongoose = require('mongoose')

let categoryControler = {
    create_Category: create_Category,
    get_Category_Of_Products: get_Category_Of_Products,
    delete_Category_Of_Products: delete_Category_Of_Products,
    get_Category: get_Category,
    update_Category_Of_Products: update_Category_Of_Products
}

//creating category
function create_Category(req, res, next) {
    const category = new Category({
        _id: new mongoose.Types.ObjectId(),
        categoryName: req.body.categoryName
    })
    Category.find({
            categoryName: req.body.categoryName
        })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.json(Gen.responseReturn(409, 'categoryName Already Exists Try Different categoryName', {}))
            } else {
                category.save().then(result => {
                    console.log(result)
                    let category = {
                        categoryName: result.categoryName,
                        _id: result._id
                    }
                    return res.json(Gen.responseReturn(200, 'Category created', category, true))
                }).catch(err => {
                    res.status(500).json(err)
                })
            }
        })
}


//Getting all Category list
function get_Category(req, res, next) {
    const pageSize = req.body.pageSize;
    const pageNo = req.body.pageNo;
    Category.aggregate([

        {
            $project: {
                categoryName: 1
            }
        },
        {
            $facet: {
                category: [{
                    $skip: (pageNo - 1) * pageSize
                }, {
                    $limit: pageSize
                }],

                totalCount: [{
                    $count: 'count'
                }]
            }
        }

    ]).exec(function (err, data) {
        res.status(200).json(data)
    })

}

//getting category with products
function get_Category_Of_Products(req, res, next) {
    const pageSize = req.body.pageSize ? parseInt(req.body.pageSize) : 10;
    const pageNo = req.body.pageNo ? parseInt(req.body.pageNo) : 1;

    const id = req.body.id
    Product.find({
            categoryId: id
        })
        .skip((pageNo - 1) * pageSize)
        .limit(pageSize)
        .populate(' categoryId ', 'categoryName')
        .exec().
    then(order => {
            if (order) {
                let len = order.length
                res.status(200).json({
                    count: len,
                    order: order

                })
            } else {
                res.status(404).json({
                    message: 'No valid entry found'
                })
            }
        })
        .catch(err => {
            res.status(500).json(err)
        })
}

//deleting category with all related products also
function delete_Category_Of_Products(req, res, next) {
    const id = req.params.categoryId
    Category.deleteOne({
            _id: id
        })
        .exec()
        .then(result => {
            Product.deleteMany({
                    categoryId: id
                })
                .exec()
                .then(result2 => {
                    res.status(200).json(result2)
                })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

function update_Category_Of_Products(req, res, next) {
    const update = new Category({
        categoryName: req.body.categoryName
    })

    Category.findOneAndUpdate({
        _id: req.body.id
    }, update, {
        new: true
    }, function (err, updated) {
        if (err) {
            return res.json(Gen.responseReturn(409, 'Something went wrong id is not correct', {}))
        } else if (updated) {

            return res.json(Gen.responseReturn(200, 'Category updated successfully', {}, true))
        } else {
            return res.json(Gen.responseReturn(200, 'Category not found', {}, true))
        }
    })


}

module.exports = categoryControler