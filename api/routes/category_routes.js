const express = require('express')
const router = express.Router()

const checkAuth = require('../middleware/check-auth')
const categoryControler = require('../controllers/category_controler')


router.post('/create', checkAuth, categoryControler.create_Category)

router.post('/list', checkAuth, categoryControler.get_Category)

router.post('/list/cat_Product', checkAuth, categoryControler.get_Category_Of_Products)

router.delete('/list/:categoryId', checkAuth, categoryControler.delete_Category_Of_Products)

router.patch('/update', checkAuth, categoryControler.update_Category_Of_Products)

module.exports = router