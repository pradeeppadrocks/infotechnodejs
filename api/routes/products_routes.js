const express = require('express')
const router = express.Router()

const checkAuth = require('../middleware/check-auth')
const productController = require('../controllers/product_controler')


router.post('/create', checkAuth, productController.create_products)


router.post('/list', checkAuth, productController.get_All_Products)

router.delete('/list/:productId', checkAuth, productController.delete_Products)

router.patch('/update', checkAuth, productController.update_Products)

module.exports = router